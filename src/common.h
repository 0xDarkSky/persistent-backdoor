#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef SOCKET_ERROR
#define SOCKET_ERROR (-1)
#endif
#define SERVER_IP ("SERVER IP")
#define SERVER_PORT (PORT)
#define CONN_AMNT (5)

#ifndef bzero
#define bzero(p, size) (void)memset((p), 0, (size))
#endif
